set nocompatible

syntax on

filetype indent plugin on

let mapleader=';'

set expandtab
set autoindent
set number
set nowrap
set fdm=indent
set shiftwidth=2
set tabstop=2
set laststatus=2
set backspace=eol,indent,start
set visualbell
set t_vb=
set background=light

set mouse=a
set pastetoggle=<F11>
set clipboard=unamedplus

set wildmenu
set wildmode=list:longest,full

set showmatch
set incsearch

" Use Pathogen for Plugins
execute pathogen#infect()
" https://github.com/scrooloose/nerdtree
" https://github.com/pangloss/vim-javascript
" https://github.com/ryanoasis/nerd-fonts
" git clone https://github.com/elixir-lang/vim-elixir.git
" git clone https://github.com/w0rp/ale.git
" git clone https://github.com/tomtom/tlib_vim.git
" git clone https://github.com/MarcWeber/vim-addon-mw-utils.git
" git clone https://github.com/garbas/vim-snipmate.git
" git clone https://github.com/honza/vim-snippets.git
" git clone https://github.com/vim-airline/vim-airline
" git clone https://github.com/vim-airline/vim-airline-themes
" git clone git@github.com:ryanoasis/vim-devicons.git
" git clone git@github.com:jiangmiao/auto-pairs.git
" git clone git@github.com:tpope/vim-commentary.git
" git clone git@github.com:tpope/vim-fugitive.git
" git clone git@github.com:digitaltoad/vim-pug.git
" git clone git@github.com:statianzo/vim-jade.git
" git clone git@github.com:jelera/vim-javascript-syntax.git
" git clone git@github.com:elzr/vim-json.git
" git clone git@github.com:mxw/vim-jsx.git
" git clone git@github.com:groenewege/vim-less.git
" git clone git@github.com:xolox/vim-misc.git
" git clone git@github.com:xolox/vim-notes.git
" git clone git@github.com:Xuyuanp/nerdtree-git-plugin.git
" git clone git@github.com:mhinz/vim-signify.git
" git clone git@github.com:rust-lang/rust.vim.git
" git clone git@github.com:leissa/vim-acme.git
" git clone https://github.com/ARM9/arm-syntax-vim.git

hi clear SignColumn
hi clear SpellBad
hi clear SpellCap
hi clear ALEErrorSign
hi clear ALEWarningSign

" Visual Selections
hi Visual ctermfg=black ctermbg=white

" Linting Styles
hi SpellBad term=underline cterm=underline ctermfg=darkred
hi SpellCap term=underline cterm=underline ctermfg=81
hi ALEErrorSign term=underline cterm=underline ctermfg=darkred
hi ALEWarningSign ctermfg=81

" Git Styles
hi SignifySignAdd cterm=bold ctermfg=darkgreen
hi SignifySignDelete cterm=bold ctermfg=darkred
hi SignifySignChange cterm=bold ctermfg=yellow

" NERDTree Key Binding (Plugin)
map <C-n> :NERDTreeToggle<CR>

" Airline Config (Plugin)
let g:airline_powerline_fonts = 1
let g:airline_theme = 'laederon'

let g:jsx_ext_required = 0

let g:ale_cpp_clang_options = '-std=c++14 -Wall -Iinclude'
let g:ale_cpp_clangd_options = '-Iinclude'
let g:ale_cpp_clangtidy_options = '-Iinclude'
let g:ale_cpp_clangcheck_options = '-Iinclude'
let g:ale_echo_msg_format = '%linter% says %s'

" NerdTree colors
function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
  exec 'autocmd FileType nerdtree highlight ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:guibg .' guifg='. a:guifg
  exec 'autocmd FileType nerdtree syn match ' . a:extension .' #^\s\+.*'. a:extension .'$#'
endfunction

call NERDTreeHighlightFile('jade', 'red', 'none', 'red', '#151515')
call NERDTreeHighlightFile('pug', 'red', 'none', 'red', '#151515')
call NERDTreeHighlightFile('html', 'red', 'none', 'red', '#151515')
call NERDTreeHighlightFile('md', 'magenta', 'none', 'blue', '#151515')
call NERDTreeHighlightFile('json', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('less', 'brown', 'none', 'cyan', '#151515')
call NERDTreeHighlightFile('css', 'brown', 'none', 'cyan', '#151515')
call NERDTreeHighlightFile('js', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('jsx', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('rs', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('purs', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('elm', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('exs', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('Dockerfile', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('yml', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('toml', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('acme', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('cpp', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('hpp', 'brown', 'none', 'cyan', '#151515')
call NERDTreeHighlightFile('Makefile', 'yellow', 'none', 'yellow', '#151515')

nnoremap <Left> :echoe "Use h"<CR>
nnoremap <Right> :echoe "Use l"<CR>
nnoremap <Up> :echoe "Use k"<CR>
nnoremap <Down> :echoe "Use j"<CR>

" line jumps
noremap H ^
noremap J 5j
noremap K 5k
noremap L g_

" git jumps
nmap  <leader>gk <plug>(signify-prev-hunk)
nmap  <leader>gj <plug>(signify-next-hunk)

" lint jumps
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" save/exiting
nnoremap <leader>q :q!<CR>
nnoremap <leader>z ZZ
nnoremap <leader>w :w<CR>
nnoremap <leader>e :e!<CR>

" tab managment
nnoremap <leader>t :tabe<CR>:NERDTreeToggle<CR>
nnoremap <leader>L gt
nnoremap <leader>H gT

" pane navigation
nnoremap <leader>l <C-w>l
nnoremap <leader>k <C-w>k
nnoremap <leader>j <C-w>j
nnoremap <leader>h <C-w>h

" indentation
nnoremap <leader><Tab> >>
nnoremap <leader><S-Tab> <<
vnoremap <leader><Tab> >
vnoremap <leader><S-Tab> <

nnoremap <leader>h <C-w>h


" just silly
map q <Nop>

autocmd Filetype elm setlocal ts=2 sw=2
autocmd Filetype acme setlocal ts=8 sw=8
autocmd BufNewFile,BufRead  *.mdx set filetype=markdown.jsx
autocmd BufNewFile,BufRead *.s,*.S set filetype=arm
